package org.fillmatrix

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class TestsFillMatrix {
    @Test
    fun `Dim 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, 0)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Dim lower than 0`() {
        val m = mock(Matrix::class.java)
        try {
            fillMatrix(m, -1)
        } catch (ex: IllegalArgumentException) {
            print(ex.message)
        }
    }

    @Test
    fun `Matrix 1 x 1`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 1)

        Mockito.verify(m).set(0, 0, 1)
    }

    @Test
    fun `Matrix 4 x 4`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 4)
        //  1, 2,  3, 4,
        //  12, 13,  14, 5,
        //  11, 16, 15, 6,
        //   10,  9,  8, 7
        Mockito.verify(m).set(0, 0, 1)
        Mockito.verify(m).set(0, 1, 2)
        Mockito.verify(m).set(0, 2, 3)
        Mockito.verify(m).set(0, 3, 4)

        Mockito.verify(m).set(1, 3, 5)
        Mockito.verify(m).set(2, 3, 6)
        Mockito.verify(m).set(3, 3, 7)

        Mockito.verify(m).set(3, 2, 8)
        Mockito.verify(m).set(3, 1, 9)
        Mockito.verify(m).set(3, 0, 10)

        Mockito.verify(m).set(2, 0, 11)
        Mockito.verify(m).set(1, 0, 12)

        Mockito.verify(m).set(1, 1, 13)
        Mockito.verify(m).set(1, 2, 14)

        Mockito.verify(m).set(2, 2, 15)
        Mockito.verify(m).set(2, 1, 16)
    }

    @Test
    fun `Matrix 5 x 5`() {
        val m = mock(Matrix::class.java)

        fillMatrix(m, 5)
//        1, 2, 3, 4, 5,
//        16, 17, 18, 19, 6,
//        15, 24, 25, 20, 7,
//        14, 23, 22, 21, 8,
//        13, 12, 11, 10, 9
        Mockito.verify(m).set(0, 0, 1)
        Mockito.verify(m).set(0, 1, 2)
        Mockito.verify(m).set(0, 2, 3)
        Mockito.verify(m).set(0, 3, 4)

        Mockito.verify(m).set(0, 4, 5)
        Mockito.verify(m).set(1, 4, 6)
        Mockito.verify(m).set(2, 4, 7)

        Mockito.verify(m).set(3, 4, 8)
        Mockito.verify(m).set(4, 4, 9)
        Mockito.verify(m).set(4, 3, 10)

        Mockito.verify(m).set(4, 2, 11)
        Mockito.verify(m).set(4, 1, 12)

        Mockito.verify(m).set(4, 0, 13)
        Mockito.verify(m).set(3, 0, 14)

        Mockito.verify(m).set(2, 0, 15)
        Mockito.verify(m).set(1, 0, 16)

        Mockito.verify(m).set(1, 1, 17)
        Mockito.verify(m).set(1, 2, 18)
        Mockito.verify(m).set(1, 3, 19)

        Mockito.verify(m).set(2, 3, 20)
        Mockito.verify(m).set(3, 3, 21)

        Mockito.verify(m).set(3, 2, 22)
        Mockito.verify(m).set(3, 1, 23)

        Mockito.verify(m).set(2, 1, 24)
        Mockito.verify(m).set(2, 2, 25)
    }
}
