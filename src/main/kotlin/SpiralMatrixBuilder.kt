package org.fillmatrix

import kotlin.properties.Delegates

/**
 *  matrix builder for spiral matrix
 */

class SpiralMatrixBuilder {
    private var dim by Delegates.notNull<Int>()
    private var sum by Delegates.notNull<Int>()
    private var lastSum = 0
    private var counter = 1
    private var branchNumber = 0
    private var elementBranchNumber = 1
    private var row = 0
    private var column = 0

    fun buildSize(dim: Int) {
        this.dim = dim
        sum = 4 * (dim - 1 - 2 * branchNumber)
    }

    fun buildCell(m: Matrix) {
        if (counter > sum) {
            branchNumber++
            lastSum = sum
            sum += 4 * (dim - 1 - 2 * branchNumber)
        }

        elementBranchNumber = counter - lastSum

        if (elementBranchNumber <= dim - 2 * branchNumber) {
            row = branchNumber
            column = branchNumber + elementBranchNumber - 1
        } else if (elementBranchNumber > dim - 2 * branchNumber && elementBranchNumber <= (sum - lastSum) / 2 + 1) {
            row = elementBranchNumber - (dim - 2 * branchNumber) + branchNumber
            column = dim - branchNumber - 1
        } else if (elementBranchNumber <= sum - lastSum - (dim - 2 * (branchNumber + 1)) && elementBranchNumber > (sum - lastSum) / 2 + 1) {
            row = dim - 1 - branchNumber
            column = dim - (elementBranchNumber - ((sum - lastSum) / 2 + 1)) - 1 - branchNumber
        } else if (elementBranchNumber <= sum - lastSum && elementBranchNumber > sum - lastSum - (dim - 2 * (branchNumber + 1))) {
            row = sum - lastSum - elementBranchNumber + 1 + branchNumber
            column = branchNumber
        }
        m.set(row, column, counter)
        counter++
    }
}
