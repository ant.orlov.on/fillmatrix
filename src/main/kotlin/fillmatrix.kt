package org.fillmatrix

import java.lang.IllegalArgumentException

/**
 * creates a matrix and fills it with elements according to a certain rule and returns it
 * fills the matrix with a spiral, example:
 * 1, 2, 3, 4, 5,
 * 16, 17, 18, 19, 6,
 * 15, 24, 25, 20, 7,
 * 14, 23, 22, 21, 8,
 * 13, 12, 11, 10, 9
 * @param matrix matrix that will be filled
 * @param dim matrix dimension
 */

fun fillMatrix(matrix: Matrix, dim: Int) {
    if (dim <= 0) {
        throw IllegalArgumentException("dim should be > 0")
    }
    val spiralMatrixBuilder = SpiralMatrixBuilder()
    spiralMatrixBuilder.buildSize(dim)
    IntRange(0, dim * dim - 1).forEach {
        spiralMatrixBuilder.buildCell(matrix)
    }
}
